FROM python:3-stretch

WORKDIR /opt/app

ENV PYTHONUNBUFFERED 1

RUN pip install livereload

CMD ["livereload", "--host", "0.0.0.0", "-p", "8000"]
